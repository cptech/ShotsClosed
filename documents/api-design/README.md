# API Design

>  *  Version: 1
>  *  Status: **Unfinished**

## Common Rules

本API尽可能遵循RESTful的原则，有以下基本规则：

*  通过token机制授权，token是jwt类型
*  使用`GET`，`POST`，`PUT`和`DELETE`四种HTTP动词
*  使用HTTP标准状态码表达请求结果
*  使用JSON格式作为数据交换格式
*  使用session机制维持登录状态，受保护资源的请求的API文档会有“Protected”条目

## Common Settings

*  Host: `http://shotsapp.perqin.com/api/`

## Content

这里包含了本文档的目录方便快速查阅。

*  [User Authentication](#user-authentication_4)
   *  [认证](#认证_5)

   *  [登出](#登出_8)

*  [Story](#story_11)

   *  [创建Story](#创建story_12)
   *  [获取Story信息列表](#获取story信息列表_13)
   *  [获取指定Story信息](#获取指定story信息_15)
   *  [修改指定Story信息](#修改指定story信息_16)
   *  [删除指定Story](#删除指定story_17)

*  [Photo](#photo_18)

   *  [添加Photo到指定Story](#添加photo到指定story_19)
   *  [获取指定Photo信息](#获取指定photo信息_20)
   *  [获取指定Story的Photo信息列表](#获取指定story的photo信息列表_21)
   *  [获取指定Story的指定Photo信息](#获取指定story的信息photo信息_22)
   *  [移除指定Story的指定Photo](#移除指定story的指定photo_23)
   *  [移除指定Photo](#移除指定photo_24)

*  [User](#user_25)

   *  [查看User信息](#查看user信息_26)
   *  [更新User信息](#更新user信息_27)
   *  [移除指定Story的指定User](#移除指定story的指定user_28)
   *  [向指定Story添加指定User](#向指定story添加指定user_29)

## User Authentication

这部分文档包含用户身份验证流程所需的API接口。

### 认证

#### 说明

不论是新注册用户验证还是已注册用户登录，统称为认证。

目前支持的认证类型有：

*  体验帐号登录
*  手机+验证码注册/登录
*  手机+密码登录

直观起见，我使用字符图片来说明认证流程。

*  体验帐号登录

   ````
   +--------------+                                  +--------------+
   |              |           Send request           |              |
   |              | -------------------------------> |              |
   |              |                                  |              |
   |              |  Respond [session_id] in cookie  |              |
   |              | <------------------------------- |              |
   |    Client    |                                  |  Web Server  |
   |              |     Request with [session_id]    |              |
   |              | -------------------------------> |              |
   |              |                                  |              |
   |              |  Respond [protected resources]   |              |
   |              | <------------------------------- |              |
   +--------------+                                  +--------------+
   ````

*  手机+验证码注册/登录

   ````
   +--------------+                                  +--------------+
   |              |        Send [phone number]       |              |
   |              | -------------------------------> |              |
   |              |                                  |              |
   |              |    Respond success status code   |              |
   |              | <------------------------------- |              |
   |              |                                  |              |
   |              |   Send [phone] and [SMS code]    |              |
   |              | -------------------------------> |              |
   |    Client    |                                  |  Web Server  |
   |              |  Respond [session_id] in cookie  |              |
   |              | <------------------------------- |              |
   |              |                                  |              |
   |              |     Request with [session_id]    |              |
   |              | -------------------------------> |              |
   |              |                                  |              |
   |              |  Respond [protected resources]   |              |
   |              | <------------------------------- |              |
   +--------------+                                  +--------------+
   ````

*  手机+密码登录

   ````
   +--------------+                                      +--------------+
   |              |  Send [phone number] and [password]  |              |
   |              | -----------------------------------> |              |
   |              |                                      |              |
   |              |    Respond [session_id] in cookie    |              |
   |              | <----------------------------------- |              |
   |    Client    |                                      |  Web Server  |
   |              |       Request with [session_id]      |              |
   |              | -----------------------------------> |              |
   |              |                                      |              |
   |              |    Respond [protected resources]     |              |
   |              | <----------------------------------- |              |
   +--------------+                                      +--------------+
   ````


一旦返回未授权，则需要用户重新登录。

#### API

*  URI: `/sessions`

*  Method: POST

*  Request body:

   *  Evaluate

      ````json
      {
        "type": "evaluation"
      }
      ````

   *  Phone (phone number)

      ````json
      {
        "type": "sms",
        "phone": "12345678901"
      }
      ````

   *  Phone (verify code)

      ```json
      {
        "type": "sms_code",
        "code": "123456",
        "phone": "12345678901"
      }
      ```

   *  Phone and password

      ````json
      {
        "type": "password",
        "phone": "12345678901",
        "password": "12345678"
      }
      ````


*  Successful response:

   *  Evaluate

      没有返回体，但是状态码为成功且有`Set-Cookie`头部。

   *  Phone (phone number)

      没有返回体，但是状态码为成功。

   *  Phone (verify code)

      没有返回体，但是状态码为成功且有`Set-Cookie`头部。

   *  Phone and password

      没有返回体，但是状态码为成功且有`Set-Cookie`头部。

*  Failing response:

   ````json
   {
     "message": "Invalid parameters"
   }
   ````



###  登出

#### 说明

这里的登出指的是用户手动点击“退出登录”选项。

#### API

*  URI: `/sessions`
*  Method: DELETE
*  Protected
*  Response: Omit

## Story

### 创建Story

#### API

*  URI: `/stories`

*  Method: POST

*  Protected

*  Request body:

   ````json
   {
     "title": "Journey to the West",
     "description": "Optional"
   }
   ````

*  Response:

   ````json
   {
     "sid": "0123456789abvdef"
   }
   ````

### 获取Story信息列表

#### API

*  URI: `/stories`

*  Method: GET

*  Protected

*  Response:

   ````json
   {
     "stories": [
       {
         "sid": "0123456789abcdef",
         "start_date": 1480264585000,
         "end_date": 1480264586000,
         "title": "Journey to the West",
         "description": "We've spent 2 weeks walking from Guangzhou to India!!!",
         "creator": "$USER$",
         "status": "closed",
         "members_count": 2,
         "members": [
           "$USER$",
           "$USER$"
         ],
         "photos_count": 2,
         "photos": [
           "$PHOTO$",
           "$PHOTO$"
         ]
       },
       {
       }
     ]
   }
   ````

   注：

   +  `$USER$`表示用户ID或用户对象，取决于参数`full_creator`和`full_members`
   +  `$PHOTOS$`表示照片ID或照片对象，取决于参数`full_photos`

### 获取指定Story信息

*  URI: `/stories/:story_id`
*  Method: GET
*  Protected
*  Response: refer to `GET /stories`

### 修改指定Story信息

*  URI: `/stories/:story_id`

*  Method: PUT

*  Protected

*  Request body:

   ````json
   {
     "title": "Journey to the East",
     "description": "Optional"
   }
   ````

*  Response body: None

### 删除指定Story

*  URI: `/stories/:story_id`
*  Method: DELETE
*  Protected
*  Response body: None

## Photo

### 添加Photo到指定Story

### 获取指定Photo信息

### 获取指定Story的Photo信息列表

### 获取指定Story的指定Photo信息

*  URI: `/stories/:story_id/photos/:photo_id`

*  Method: GET

*  Protected

*  Response body:

   ````json
   {
     "pid": "0123456789abcdef",
     "thumbnail_size": 4096,
     "thumbnail_url": "/files/abcDEFghiJKL0.png",
     "preview_size": 102400,
     "preview_url": "/files/abcDEFghiJKL1.png",
     "origin_size": 4096000,
     "origin_url": "/files/abcDEFghiJKL2.png"
   }
   ````

### 移除指定Story的指定Photo

### 移除指定Photo

## User

### 查看User信息

### 更新User信息

### 移除指定Story的指定User

### 向指定Story添加指定User



