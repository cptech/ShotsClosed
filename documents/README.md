# Documents

This directory includes all documents related to the developments, such as the design documents and architecture documents.

## Content

*  [Server Architecture](server-architecture/README.md): Server architecture design
*  [Database Design](database-design/README.md): Database design
*  [API Design](api-design/README.md): API design document

