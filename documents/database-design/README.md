# Database Design

## Overview

本项目后端使用MEAN开发栈，使用MongoDB作为持久数据库，Redis作为session存储。

## Entity

### User

#### answer to this questionDescription

代表一个用户单位，包含用户的个人信息（手机号、昵称等），所参与的Activity的ID，所拥有的Photo（包含个人和Activity中的）的ID。

uid是用户唯一ID，在需要引用该用户的时候存储；用户的ID暂定使用手机号/体验ID，与uid不同，类似于“用户名”。

*TBD：MongoDB有reference特性，可以考虑保存引用而非ID。*

#### Schema

// TODO

### Session

#### Description

代表一个用户的一个登录会话，存储在Redis中，包含登录用户的uid和认证信息（token、失效时长等）。

#### Structure

// TODO

### Photo

#### Description

代表一张照片的信息（不是文件）。包含照片的文件信息、下载链接（缩略图、预览图、原图等）、归属User和Activity。

#### Schema

// TODO

### Activity

*TBD：暂定Activity，名称可能会改动*

#### Description

代表一个活动相册的信息，包含相册的创建人、参与人、包含的照片等信息。

#### Schema

// TODO