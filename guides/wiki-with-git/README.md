# Guide: Wiki with Git

本教程旨在提供从无到有的Git与Wiki文档管理一条龙服务。

## Git: WHAT

*本节介绍Git，已经知道的可以跳过本节。*

Git是一个版本控制系统，通过它可以管理一个项目仓库。

让我们从头了解一下。如果你正在开发一个项目PyShop，你想尝试一个新的第三方库，于是你导入了它，没想到写了两天发现这辣鸡库并不好用还一大堆坑，你决定放弃使用，那么现在问题来了：你还记得两天前你的代码长什么样么？

为了对一个项目（一般是一份源码）的每次改动都掌控于心，就有了版本控制系统（VCS）。

对于Git而言，它在某个项目的目录里管理这个项目，这个项目目录就被称作仓库（repository，缩写repo）。Git会在这个项目目录里新建一个目录`.git`，这里面会对这个项目的每一个文件的每一次改动、提交进行存储，因此你可以随意前滚后滚托马斯全旋……

Git的很多特性我们会在用到的时候再说，这里说一下必知名词。

*  add与stage（咦好像是这么拼）：在Git管理的repo中，有一个区域叫做`stage`，可以成为缓存区。当你对repo里的内容做了改动之后，通过add命令（具体的Git使用会在后文提到）可以将你的改动缓存，此时Git会把你的改动放进stage，准备提交。当然，改动的文件被缓存之后还能继续改，add之后会将最新的改动放进stage。
*  commit：commit会把stage里的内容提交到历史中，可以认为此时Git为你生成了一份快照，记录了这个提交发生的改动。
*  branch：Git有分支的概念，简单地说，想象一条时间轴，commit就是上面的事件，branch那就是从某个commit上面分出一条时间轴，后面两个时间轴还可以合并，主要用于多人写作开发！

## Git: HOW

*本节介绍Git的基本使用，如果已经会了可以直接跳过。*

通过前文我们知道，具体到你的PC上，Git是一个程序，它在你的项目里搞了一个文件夹`.git`，并通过它来追踪这个项目的版本历史。

### 安装

接下来我们学习如何获取Git。Linux用户建议通过包管理器获取，Windows用户可以访问[Git - Downloads](https://git-scm.com/downloads)下载。安装过程我就省略了，一路确定一般没问题。

对于Linux，直接在Terminal就可以使用git命令；对于Windows用户，你会发现空白处的右键菜单多了`Git Bash`选项，可以从这里进入Git为你提供的Terminal并在里面执行git命令（当然也可以执行一些比较基本的Unix命令，如`ls`之类的）。

以下内容都在Terminal环境中进行。

初次使用需要配置用户名和邮箱，建议使用你在开源中国或GitHub使用的信息（因为我也不知道否则会怎样- -）：

```shell
git config --global user.name "perqin"
git config --global user.email "perqinxie@gmail.com"
```

### 建立repo

建立repo有两种方式：将已存在的项目（不存在的项目可以看做是内容为空的已存在项目……）纳入Git管理；从Git服务器clone。

*  要将某个项目目录纳入Git管理，首先进入到项目所在的目录，然后输入

   ```shell
   git init
   ```

   就可以看到Git为你新建的仓库。

   ![git-init](git-init.png)

*  clone。你可以选择把一个被Git管理的repo同步到专门的服务器上，GitHub就是提供Git托管服务器的网站之一。这样，你可以随时随地将你的repo拉取下来，使用clone：

   ```shell
   git clone https://git.oschina.net/perqin/MobileRobotCourse.git
   ```

   其中后面的链接就是你的远程repo的地址，这个地址可以从你所使用的Git托管服务的项目页面看到，拿开源中国为例：

   ![](git-oschina-repo-page.png)

   注意SSH是另一种协议，你可以选择HTTPS或SSH之一来建立与服务器同步的连接方式，使用HTTPS的时候，每一次推送到服务器都需要输入你在这个网站的账号和密码（如果你clone的是公开的repo则不需要）。而SSH需要你生成一对公钥、私钥，然后提供公钥给网站，然后就可以通过这个验证你的身份了，由于这需要额外的配置，我就不赘述了，有兴趣的话可以自己搜索。

### 查看状态

到这里，你就可以进入到你的项目目录里，查看Git的状态了：

```shell
git status
```

这个命令可以列出你所在的分支、你的项目里那些文件发生了改动、那些改动缓存了、比服务器多了多少个新的commit等等。

### 基本操作：缓存改动

当你对代码做了改动之后，可以通过add命令把改动缓存以待提交：

```shell
git add path/to/file/filename.with.extension
```

如上图，add后面是文件名，你可以用空格分隔，写一串文件名。如果你想把当前的所有改动都提交，可以使用：

```shell
git add .
```

### 基本操作：提交改动

改动可以多次缓存到stage，当你准备提交的时候（比如修好了一个bug或者写好了一个bug），在缓存了它们之后可以使用commit来提交：

```shell
git commit -m "Create 3 new bugs"
```

-m指定这次提交的附带信息，且每次提交必须带有信息，用来描述你的本次改动。

### 基本操作：绑定远程仓库

如果你的项目是本地新建的，那么你需要绑定到一个远程repo，才能推送同步上去。首先在GitHub或其他服务器上新建repo，新建过程不赘述，但要注意建议勾上”使用README初始化项目“，这样你的项目新建好之后会自动包含一个`README`文件。如果你的项目里什么文件都没有，被称为裸仓库，在处理的时候需要特殊对待，因此尽量通过README避免这种情况。

新建好之后，用一下命令可以把远程仓库绑定到你的本地repo：

```shell
git remote add origin https://git.oschina.net/perqin/MobileRobotCourse.git
```

注意这里的origin是你给这个远程repo指定的别名。一个本地repo可以有多个远程repo，而clone下来的repo默认会为你添加你clone时候的repo为远程仓库，并默认命名为origin。

### 基本操作：拉取更新

当别人将项目更新推送到服务器之后，或者在你刚为你的项目绑定一个你刚新建的远程repo之后，你可以通过pull把新内容拉取并合并下来：

```shell
git pull origin master
```

其中origin就是前面说的远程repo的别名啦，master是默认分支（每个repo默认都会有一个master分支），这句命令会把远程repo`origin`的master分支的更新内容拉取下来并合并到你当前的内容。换句话说，别人更新的代码文件会直接代替你的项目中相应的旧文件，而你原本正在修改的新文件改动也不会受到影响！但是如果你的改动文件正好是别人更新的文件，那么就有conflit需要处理。这里我们不详细说conflit处理，毕竟这是麻烦事，我的建议是每次开干之前先pull一下，每次干完就push上去（后面会说）。

### 基本操作：推送更新

当你本地完成并提交了之后，你可以把你的改动推送上去：

```shell
git push origin master
```

这里我就不再解释了，和前面是差不多的。当然，如果有冲突会推送失败，此时就要pull下来处理冲突了。

### 关于.gitignore

你可能已经发现，项目里还有一个`.gitignore`文件。

我们在开发过程中经常生成临时文件，如C++开发过程中的.o文件、各IDE新建的文件等。把他们加入到`.gitignore`文件中，在使用status、add等命令的时候这些文件的改动就会被忽视。`.gitignore`的语法可以自行搜索，通过`*`通配符和文件名可以覆盖大多数情况。

需要注意的是，已经被add或commit的文件由于已经被纳入管理，此时无法通过更改`.gitignore`来忽略，需要更多操作，具体依然可以自行搜索～

## Wiki: WHAT

接下来说说Wiki。在GitHub、开源中国等Git托管服务的远程repo中，提供了Wiki功能，你可以用markdown语法为某个repo添加Wiki页面，比如如何编译部署、使用方法等等。

而它们管理Wiki的方法也是用Git，比如MobileRobotCourse，它的Git地址是：

```
https://git.oschina.net/perqin/MobileRobotCourse.git
```

而它的Wiki地址则是：

```
https://git.oschina.net/perqin/MobileRobotCourse.wiki,git
```

你只需要到这个repo的页面里，切换到Wiki界面，就能添加首页，然后就可以clone下来编辑了（当然，在线编辑也是可以的）。

看到这里，我们就知道了，Wiki本质上也是一个repo，那么你可以把你用到的图片资源直接放这个repo里，然后在markdown中引用它，最后把它们一并push上去，就可以轻松的在Wiki上写文档啦！

事实上，你看到的这篇文章就是这么搞的，欢迎clone下来学习git使用～～