# 手机平台应用开发实验报告

* 年级：2014
* 方向：嵌入式软件设计
* 学号：14331304
* 姓名：谢昌秦
* 日期：2016年11月13日

## 实验题目
数据存储（一）

## 实验目的
1. 学习 SharedPreferences 的基本使用。
2. 学习 Android 中常⻅的文件操作方法。
3. 复习 Android 界面编程。

## 实验环境
* 操作系统：Ubuntu 16.04 LTS
* 开发工具：Android Studio 2.3 Canary
* 项目配置：
    + 最低SDK：19
    + 目标SDK：25
    + 依赖：
        * `com.android.support:appcompat-v7:25.0.0`
        * `com.android.support:design:25.0.0`
        * `com.android.support:support-v4:25.0.0`

## 实验内容

本次实验涉及的存储主要是SharedPreferences和File的IO，前者相对简单，后者和Java里的差不多（毕竟Android里就是一个个Java虚拟机），所以这次项目我再一次尝试使用MVP模式，这次使用的是比较正统的、基于官方提供的例子的MVP模式（参见参考链接）。

### 契约类编写

**由于MVP并非本次实验的要求，而是基于实验过程的一部分才记录下来，因此相关过程记录可以直接跳过不影响后面的内容阅读。**

在Google提供的基础MVP实现中，V由Fragment担任，M一般是一个数据仓库，P作为中间的粘合剂。

先考虑密码界面，对于View而言，他需要提供以下接口：

*  显示相关的Toast（密码不匹配等）
*  显示双栏创建密码或单栏验证密码的布局
*  跳转到Content文本编辑界面

而Presenter的接口用于响应用户操作，我们可以得出可能的操作：

*  用户点击OK按钮

于是，我们有以下契约类：

```java
package com.perqin.experimentseven.contract;


import com.perqin.experimentseven.presenter.BasePresenter;
import com.perqin.experimentseven.view.BaseView;

public interface LockContract {
    interface Presenter extends BasePresenter {
        void submit(String newPassword, String passwordConfirmed, String password);
    }

    interface View extends BaseView<Presenter> {
        void switchToContent();

        void showPasswordCreation();

        void showPasswordVerification();

        void showPasswordInvalidNotice();

        void showPasswordMismatchedNotice();

        void showPasswordEmptyNotice();
    }
}
```

需要注意的是，View里面是几乎不包含逻辑的，所以View不能决定用户点击OK调用`somePresenter.submit()`的时候应该选择哪个文本框的内容作为参数，所以可以看到，`submit`方法需要三个参数。

`ContentContract`同理，简单分析用户操作和UI反馈之后，就能很快设计好接口，因此不再贴代码，详见工程源码文件夹。

### 数据仓库接口设计

数据仓库的接口设计比较麻烦，因为它和用户交互是不相关的，不能根据应用的功能去直接设计。设计成接口也是有好处的，这意味着只要你能实现这个接口，不论你用的是文本文件、SharedPreferences或者数据库，都能直接使用。

我们可以认为，这个密码数据仓库至少需要：

*  设置新密码
*  判断密码是否已经设置
*  验证某个字符串是否是密码

于是我们有以下接口：

```java
public interface UserDataSource {
    boolean verifyPassword(String password);
    void setPassword(String password);
    boolean passwordSet();
    boolean passwordVerified();
    void requestPasswordVerified();
}
```

需要注意的是，我这里认为是否已经通过密码验证也应该被存储，只是存储在内存中，因此增加了两个相关接口。

### View的实现

由于Fragment作为View，因此Activity就变成一个壳，负责Presenter和Fragment的初始化，初始化代码如下：

```java
@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    LockFragment lockFragment = (LockFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    if (lockFragment == null) {
        lockFragment = LockFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, lockFragment)
                .commit();
    }

    mPresenter = new LockPresenter(UserRepository.getInstance(this), lockFragment);

    lockFragment.setPresenter(mPresenter);
}
```

其中Activity的布局中只有一个FrameLayout作为Fragment的容器。

Fragment需要实现View的相关方法，这里不贴代码，仅说说大概内容：

UI中的文本框我连续使用了三个，而相关的View接口会将其中用于创建密码的两个或用于验证密码的一个有选择的隐藏或显示（通过`View.setVisibility()`）。我不妨认为Activity刚显示也算用户操作，因此所有的Presenter都有`start()`和`stop()`方法，分别于Fragment恢复和暂停的时候调用，在这里就可以调用对应的View方法设置如何显示文本框了。

### Presenter的实现

Presenter中需要实现相关的逻辑，其实有了前面的契约和数据仓库接口设计之后，Presenter的实现就水到渠成了，以`start`方法为例：

```java
@Override
public void start() {
    if (mUserRepository.passwordSet()) {
        mView.showPasswordVerification();
    } else {
        mView.showPasswordCreation();
    }
}
```

代码非常地明白：调用持有的数据仓库实例判断用户创建密码了没有，然后调用View实例显示相应的UI。

这里不再赘述，包括Content部分，因为两者的流程和结构大同小异，接下来将主要说明数据仓库的实现。

### Lock数据仓库实现

Lock仓库通过SharedPreferences保存密码。

在创建密码的接口中，使用`edit`方法然后调用`putString`：

```java
@Override
public void setPassword(String password) {
    mSharedPreferences.edit()
            .putString(PK_PASSWORD, new MD5(password).getMD5edString())
            .apply();
    mPasswordVerified = true;
}
```

其中`MD5`是我写的辅助类，通过MD5加密来一定程度上保证密码安全性。这里使用`apply`而非`commit`是因为官方推荐，因为`apply`是异步的，避免卡顿。

在验证密码的时候，使用`getString`即可：

````java
@Override
public boolean verifyPassword(@NonNull String password) {
    if (new MD5(password).getMD5edString().equals(mSharedPreferences.getString(PK_PASSWORD, null))) {
        mPasswordVerified = true;
        return true;
    } else {
        return false;
    }
}
````

另外，可以通过`contains`方法判断是否设置了密码：

```java
@Override
public boolean passwordSet() {
    return mSharedPreferences.contains(PK_PASSWORD) && (mSharedPreferences.getString(PK_PASSWORD, null) != null);
}
```

### Content数据仓库的实现

在Content文本编辑界面，数据仓库至少需要实现文本的读取和写入。

首先，在构造函数需要判断文件可访问性。为了避开权限，我限制文件位置在`SDCard/Android/data/com.perqin.experimentseven/files/config.txt`中，这个位置对应用来说不需要SD卡读写权限。

```java
private ContentRepository(Context context) {
    if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
        mFile = new File(context.getExternalFilesDir(null), FILE_NAME);
        try {
            // Create the file if it doesn't exist, check availability otherwise
            mFileAvailable = (!mFile.exists() && mFile.createNewFile())
                    || (mFile.exists() && mFile.isFile() && mFile.canRead() && mFile.canWrite());
        } catch (IOException e) {
            e.printStackTrace();
            mFileAvailable = false;
        }
    } else {
        mFileAvailable = false;
    }
}
```

如上，通过Java提供的File类判断是否可访问，并预先创建好文件。

在读取文件的时候，首先构造FileInputStream类得到文件输入流，然后得到流读取器InputStreamReader，将内容一次1KB循环读取直到读取完毕。

```java
@Override
public boolean loadFile() {
    if (!mFileAvailable)
        return false;
    try {
        FileInputStream fileInputStream = new FileInputStream(mFile);
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, Charset.forName("UTF-8"));
        StringBuilder stringBuilder = new StringBuilder();
        char[] buffer = new char[1024];
        int length;
        while ((length = inputStreamReader.read(buffer)) != -1) {
            stringBuilder.append(buffer, 0, length);
        }
        fileInputStream.close();
        mFileContent = stringBuilder.toString();
        return true;
    } catch (IOException e) {
        e.printStackTrace();
        return false;
    }
}
```

在写入文件的时候，同理获得输出流和流写入器，写入之后记得`flush`把缓冲区清空。

````java
@Override
public boolean saveFile() {
    if (!mFileAvailable)
        return false;
    try {
        FileOutputStream fileOutputStream = new FileOutputStream(mFile, false);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, Charset.forName("UTF-8"));
        outputStreamWriter.write(mFileContent);
        fileOutputStream.flush();
        outputStreamWriter.close();
        return true;
    } catch (IOException e) {
        e.printStackTrace();
        return false;
    }
}
````

## 实验结果

实验截图如下。

<pagebreak/>



<img src="mismatch.png" height="640" width="360"/>

_密码不匹配_

<pagebreak/>
<img src="verification.png" height="640" width="360"/>

_验证密码_

<pagebreak/>

<img src="save.png" height="640" width="360"/>

_存储文件_

<pagebreak/>

<img src="load.png" height="640" width="360"/>

_加载_

<pagebreak/>

<img src="saved-file.png" height="640" width="360"/>

_被写入的文件（文件管理器视图）_

<pagebreak/>

<img src="saved-content.png" height="640" width="360"/>

_写入的文件（由于截图时操作不当，截图中的文本是后来重新写入截的，而前面的App界面结果是一开始写入截取的，由于是语音输入，可能有小部分出入）_

## 实验总结

本次实验主要学习了文件读写和SharedPreferences。后者可以认为就是一个App内有效的键值对集合，所以非常适合类似App设置之类的简单数据存储。至于密码安全性的问题，我用以前写过的一个简单的MD5的类解决。

文件读写部分，主要还是用的Java提供的包，获取文件输入输出流，然后用读写器读写，这里读和写还有一点点去别。读的时候，我一开始用了BufferedReader，它可以一次读取一行，结果发现这样会去掉换行符，所以就自己加了个char数组作为buffer，循环读取；而写的时候则要注意flush掉缓冲区。

最后还遇到一个坑。我增加了一个特性：从内容界面按Home后台之后，重新切换到App会弹出密码验证界面，这里直接复用了Lock界面，但是问题在于此时在Lock界面不输入密码直接返回的话，会重新回到内容界面，而由于密码未验证，此时内容界面会看作你是切换回来的，又会打开界面，由此导致循环。最后的解决方法比较奇葩，在密码界面监听返回按钮，按下之后切回后台。这样的问题也是显而易见的：App无法被真正退出而只会回到后台。

顺便提一提MVP。一旦想清楚了一个界面需要的操作，MVP的接口设计就简洁明了，接口实现也水到渠成，而且方便测试，之前曾经尝试过一次，但是只是自己瞎写，这次仿照Google提供的示范，尤其是数据仓库部分的示范，可以说写得很顺手。

## 参考链接

*  Google的MVP框架示范：[googlesamples/android-architecture at todo-mvp](https://github.com/googlesamples/android-architecture/tree/todo-mvp/)

