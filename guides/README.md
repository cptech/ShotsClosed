# Guides

This directory contains useful guides and tutorials collected from the Internet and contributed by our team members.

## Content

*  [Wiki with Git](wiki-with-git/README.md): Write your documents with markdown syntax and publish them to git repository for sharing and collaboration.
*  [MVP Pattern in Android](mvp-pattern-in-android/README.md): Learn MVP pattern in Android from Perqin's lab report of the 7th lab.